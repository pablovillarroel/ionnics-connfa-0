import { Directive, Input, ElementRef, Renderer } from '@angular/core';
import { Header } from 'ionic-angular';

/**
 * Generated class for the HideHeaderDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[hide-header]', // Attribute selector
  host:{
    '(ionScroll)': 'onScrollEvent($event)'
  }
})
export class HideHeaderDirective {
  @Input("header") header: HTMLElement;
  headerHeight;
  scrollContent;

  constructor(public element: ElementRef, public renderer: Renderer) {
    console.log('Hello HideHeaderDirective Directive');
  }

  ngOnInit(){
    /*this.renderer.setElementStyle(this.header,'webkitTransition', 'top 700ms');*/
    this.scrollContent = this.element.nativeElement.getElementsByClassName("scroll-content")[0];
    this.renderer.setElementStyle(this.scrollContent,'top', '-60px');
    this.renderer.setElementStyle(this.header,'opacity', '0.45');
    this.renderer.setElementStyle(this.scrollContent,'margin', '0px');
  }

  onScrollEvent(event){
    if(event.scrollTop > 65){
      this.renderer.setElementStyle(this.header,'opacity', '0.90');
      /*this.renderer.setElementStyle(this.scrollContent,'top', '-56px');*/
    }else if(event.scrollTop > 60){
      this.renderer.setElementStyle(this.header,'opacity', '0.80');
      /*this.renderer.setElementStyle(this.scrollContent,'margin-top', '0px');*/
    }else if(event.scrollTop > 50){
      this.renderer.setElementStyle(this.header,'opacity', '0.75');
    }else if(event.scrollTop > 40){
      this.renderer.setElementStyle(this.header,'opacity', '0.65');
    }else if(event.scrollTop > 30) {
      this.renderer.setElementStyle(this.header,'opacity', '0.60');
    }else{
      this.renderer.setElementStyle(this.header,'opacity', '0.45');
    }

  }

}
