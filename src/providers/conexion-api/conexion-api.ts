import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as ENV } from '../../environments/environment' ;
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
/*
  Generated class for the ConexionApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConexionApiProvider {

  public url_final = ENV.urlbase.concat(':', ENV.port, '/api/', ENV.n_conferencia);

  constructor(public http: HttpClient) {
  }

  public obtenerInformacion(nombre){
    let url_get = this.url_final.concat(nombre);
    let res = this.http.get(url_get);
    return res;
  }

}
