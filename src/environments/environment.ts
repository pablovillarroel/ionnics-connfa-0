
export var environment = {  
    ////////////////////////////////////////////
    //////////// Conexión a API ///////////////
    ///////////////////////////////////////////

    // Raiz de la URL
    urlbase : "http://lambda.inf.ucv.cl",
    // Puerto de la URL Ejemplo:8080 
    port : "8091",
    // ID asociada a la conferencia                        
    n_conferencia : "3",                  

    //Variables 
    // Tiempo maximo de espera en carga de las APIS
    maxTimeOut : 7000,
    // URL de las imagenes                   
    imagenMenu :"https://www.spegc.org/wp-content/uploads/2016/06/Congreso-Turismo.jpg",
    imagenSesion :"http://catedratelefonica.ulpgc.es/sites/default/files/ctd_positivo.jpg",
    imagenConfer :"http://catedratelefonica.ulpgc.es/sites/default/files/ctd_positivo.jpg",
    nombredia : 'es-MX',
    api_Google_map: null,

}