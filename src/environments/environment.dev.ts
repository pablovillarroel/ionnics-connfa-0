export var environment = {  
    
    ////////////////////////////////////////////
    //////////// Conexión a API ///////////////
    ///////////////////////////////////////////

    // Raiz de la URL
    urlbase : "http://lambda.inf.ucv.cl",
    // Puerto de la URL Ejemplo:8080 
    port : "8091",
    // ID asociada a la conferencia                        
    n_conferencia : "3",                  

    //Variables 
    // Tiempo maximo de espera en carga de las APIS
    maxTimeOut : 7000,                     
    imagenMenu : "https://via.placeholder.com/450x200",
    imagenDetalles : "https://via.placeholder.com/350x65",
    nombredia : 'es-MX',
}