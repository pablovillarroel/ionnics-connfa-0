import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BofPage } from './bof';

@NgModule({
  declarations: [
    BofPage,
  ],
  imports: [
    IonicPageModule.forChild(BofPage),
  ],
})
export class BofPageModule {}
