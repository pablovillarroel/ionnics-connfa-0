import { Component,ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, Slides,LoadingController, SegmentButton  } from 'ionic-angular';

import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';

import { environment as ENV } from '../../environments/environment' ;

import { DetalleSPage } from '../../pages/detalle-s/detalle-s';

import _ from 'lodash';

import 'rxjs/add/operator/timeout';

/**
 * Generated class for the BofPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-bof',
  templateUrl: 'bof.html',
})
export class BofPage {
  @ViewChild(Slides) slider: Slides;
  @ViewChild(SegmentButton, { read: ElementRef } ) searchElementRef: ElementRef;
  @ViewChild(SegmentButton) botonesFecha: SegmentButton;
  seleccionadoSegment: string;
  slides: any;
  page = 0;
  sesiones: any;
  eventos:any;
  fechas = new Array;
  event =  new Array;
  conferencista = new Array;
  tracks= new Array;
  confer = new Array;
  params: any;
  error = null;
  public title = new Array;
  groupSesion = [];
  loading = this.loadingCtrl.create({
    content: 'Cargando'
  });
  presentLoadingDefault() {
  
    this.loading.present();
  
  }
  constructor(public navCtrl: NavController, public proveedor: ConexionApiProvider, public loadingCtrl: LoadingController) { 
    
    this.presentLoadingDefault() ;
    

  }

  ionViewDidLoad(){

    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers'];},
      (error)=> {console.log(error);this.error = 1;}
    );

    this.proveedor.obtenerInformacion('/getTracks/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.tracks = data['tracks'];},
      (error)=> {console.log(error);this.error = 1;}
    );

    if(this.error == 1){
        this.loading.dismiss();
    }else{
      this.proveedor.obtenerInformacion('/getbofs/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.sesiones = data['days']; this.filtrar_datos();},
        (error)=> {console.log(error);this.error = 1; this.loading.dismiss();}
      );
    }
    
  }

  ngAfterViewInit() {
    this.slider.autoHeight = true; 
  }
  
  public filtrar_datos(){
    let cont;
   
    for (var i=0; i < this.sesiones.length; i++){
      for (var j=0; j < this.sesiones[i].events.length; j++){ 
        if (this.sesiones[i].events[j].deleted == true){
          delete this.sesiones[i].events[j];
        }
      }
    }
    for (i=0; i < this.sesiones.length; i++){
        cont=0;
      for (j=0; j < this.sesiones[i].events.length; j++){ 
        if(this.sesiones[i].events[j] === undefined){
          cont++;
        }
      }
      if (cont == this.sesiones[i].events.length){
        delete this.sesiones[i];
      }else{
        this.sesiones[i] = _.orderBy(this.sesiones[i],'from','asc');
      }
    }
      
    this.eventos = this.sesiones.filter(val => !!val);
    this.eventos = _.orderBy(this.eventos,0,'asc');

    for( i = 0; i < this.eventos.length; i++){
      if(this.eventos[i][0] != undefined){
        this.fechas.push(this.diaSemana(this.eventos[i][0]) + ' ' + this.eventos[i][0].substr(0, 2) );
        this.event.push(this.eventos[i][1]);
      }
    }
    for ( i = 0; i < this.event.length; i++){
      this.event[i] = this.event[i].filter(val => !!val);
    }
    for(i = 0 ; i < this.event.length ; i++ ){
      this.event[i] = _.orderBy(this.event[i],['from','to'],'asc');
    }

    for ( i = 0; i < this.event.length; i++){
      for( j = 0; j < this.event[i].length; j++){

        var dateInicio = new Date(this.event[i][j]['from']);
        var dateFinal = new Date(this.event[i][j]['to']);
        this.event[i][j]['from'] = dateInicio.getHours() + ':' + (dateInicio.getMinutes()<10?'0':'') + dateInicio.getMinutes();
        this.event[i][j]['to'] = dateFinal.getHours() + ':' + (dateFinal.getMinutes()<10?'0':'') + dateFinal.getMinutes(); 
        
        if((this.event[i][j]['speakers']).length > 0){
          this.event[i][j]['speakersNames'] = new Array;
          this.event[i][j]['speakersImagen'] = new Array;
          cont = 0;
          for( var k = 0; k < this.event[i][j]['speakers'].length ; k++){
            for(var l = 0; l < this.conferencista.length ; l++){
              if( (this.conferencista[l]['speakerId']) == (this.event[i][j]['speakers'][k]) ){
                console.log(this.event[i][j]['speakers']);
                if(this.conferencista[l]['deleted'] == false){
                  this.event[i][j]['speakersNames'][cont] = (this.conferencista[l]['firstName'] + ' ' +this.conferencista[l]['lastName']);
                  this.event[i][j]['speakersImagen'][cont] = (this.conferencista[l]['avatarImageURL']);
                  cont++;  
                }else{
                    this.event[i][j]['speakers'][k] = 0;
                  }
                  
                }
                console.log(this.event[i][j]['speakers']);
                this.event[i][j]['speakers'] = this.event[i][j]['speakers'].filter(val => !!val);
              }
            }
          }

          for( k = 0; k < this.tracks.length; k++){
            if((this.tracks[k]['trackId']) == (this.event[i][j]['track'])){
              if(this.tracks[k]['deleted'] !=  true){
                  this.event[i][j]['trackN'] = this.tracks[k]['trackName'];
                  k = this.tracks.length - 1;
              }
            }
          }

        }
      }
      console.log("Hola, estoy aqui borracho y locooooo!!!");
      console.log(this.event);
      this.groupEventos(this.event);
      this.loading.dismiss();  
  }
  public selectedTab(index) {
      this.slider.slideTo(index);
  }
  
  public goTo(sbf) {
    sbf = sbf || null;
    this.navCtrl.push(DetalleSPage, {
      data: sbf
    });
  }

  public selecOpcion(index, e) {  
    if (this.fechas[index] != null) {
      this.seleccionadoSegment = this.fechas[index]; 
      //por alguna razon solo funciona con "tap" or "click" y no con "ionSelected" 
      let segments = e.target.parentNode.children;
      let len = segments.length;
      for (let i=0; i < len; i++) {
        segments[i].classList.remove('segment-activated');
      }
      e.target.classList.add('segment-activated');
      }
  }

  public diaSemana(x) {
    console.log(x);
    x = ( x.substr(6, 4) +'/'+ x.substr(3, 2) +'/'+ x.substr(0, 2));
    console.log(x);
    let date = new Date(x);
    console.log(date);
    let dia;
  
    let options = {
      weekday: 'long',
    };
    dia = date.toLocaleDateString(ENV.nombredia, options);
    
    if(dia =='lunes') return 'Lun';
    else if(dia =='martes') return 'Mar';
    else if(dia =='miércoles') return 'Mié';
    else if(dia =='jueves') return 'Jue';
    else if(dia =='viernes') return 'Vie';
    else if(dia =='sábado') return 'Sab';
    else if(dia =='domingo') return 'Dom';
    console.log(dia);
  
  }

  public groupEventos(c){
    var i, j;
    let fecha = false;
    for ( i = 0; i < c.length ; i++){
      this.title[i] = new Array;
        for(j = 0; j < c[i].length; j++){
          if(fecha != c[i][j]['from']){
            fecha = c[i][j]['from'];
            this.title[i].push(c[i][j]['from']);
          }else{
            this.title[i].push('');
          }
        }
    }    
  }
 

}
