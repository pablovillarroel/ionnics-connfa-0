import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';

import { environment as ENV } from '../../environments/environment' ;
import _ from 'lodash';
import { isRightSide } from 'ionic-angular/umd/util/util';

/**
 * Generated class for the PlanosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-planos',
  templateUrl: 'planos.html',
})
export class PlanosPage {
  public someData : Array<{floorPlanId: string, floorPlanName: string, floorPlanImageURL : string}>;
  public planos: any;
  error = null;
  public urlFloorImage:string='/assets/imgs/none_1.jpg';


  constructor(public navCtrl: NavController,public proveedor: ConexionApiProvider, public navParams: NavParams) {
    this.someData = [];
  }

  ionViewDidLoad() {
    this.proveedor.obtenerInformacion('/getFloorPlans/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.planos = data['floorPlans']; this.filtrar_datos();},
      (error)=> {console.log(error);this.error = 1;this.funcion_error();}
    );
  }

  funcion_error(){
    if(this.error){
      console.log("entre");
        this.someData = [
          {floorPlanId: "-1", floorPlanName:"No existen datos", floorPlanImageURL:"selected"},
        ];
    }
  }

  public monthfilter(filtermonthwise){
    console.log(filtermonthwise) ;//show nothing 
    this.urlFloorImage = filtermonthwise;
    }

  public filtrar_datos(){
    for (var i = 0; i < this.planos.length; i++){
      if(this.planos[i]['deleted'] == true){
        delete this.planos[i];
      }
    }

    this.planos = this.planos.filter(val => !!val);
    this.planos = _.orderBy(this.planos,['floorPlanId'],'asc') ;

    for (i = 0; i < this.planos.length; i++){
      this.someData[i] =  this.planos[i];
    }
    this.urlFloorImage = this.someData[0]['floorPlanImageURL']
    
  }

}
