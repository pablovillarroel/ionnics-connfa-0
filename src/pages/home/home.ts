import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Slides, LoadingController, SegmentButton, ToastController } from 'ionic-angular';
import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';
import { AlertController } from 'ionic-angular';

import { environment as ENV } from '../../environments/environment' ;

import { DetalleSPage } from '../../pages/detalle-s/detalle-s';

import _ from 'lodash';
import 'rxjs/add/operator/timeout';
import { Observable }   from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {
  
  @ViewChild(Slides) slider: Slides;
  @ViewChild(SegmentButton, { read: ElementRef } ) searchElementRef: ElementRef;
  @ViewChild(SegmentButton) botonesFecha: SegmentButton;
  seleccionadoSegment: string;
  slides: any;
  page: string = "0";
  sesiones: any;
  fechas = new Array;
  event =  new Array;
  conferencista = new Array;
  tracks= new Array;
  confer = new Array;
  params: any;
  error = null;
  public title = new Array;
  groupSesion = [];
  loading = this.loadingCtrl.create({
    content: 'Cargando'
  });
  observableVar: Subscription;
  refreshInfo: Subscription;
  conferencista2: any;
  eventos: any;
  tracks2: any;
  sesiones2 = new Array;
  event2 = new Array;
  title2 = new Array ;
  fechas2 = new Array;
  toast = null;
  iconoGuardado = "ios-star-outline";
  testCheckboxOpen;
  testCheckboxResult = new Array;
  filtros: any[];

  


  presentLoadingDefault() {
  
    this.loading.present();
  
  }
  constructor(public alertCtrl: AlertController, private toastCtrl: ToastController, public navCtrl: NavController, public proveedor: ConexionApiProvider, public loadingCtrl: LoadingController) { 
      this.filtros = new Array;
      this.presentLoadingDefault() ;
      this.observableVar = Observable.interval(999).subscribe(()=>{
        this.slideChanged();
      });
      this.refreshInfo = Observable.interval(20000).subscribe(()=>{
      this.tracks2 = new Array;
      this.sesiones2 = new Array;
      this.event2 = new Array;
      this.title2 = new Array ;
      this.fechas2 = new Array;
      
      this.proveedor.obtenerInformacion('/getSpeakers/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.conferencista2 = data['speakers'];
        this.proveedor.obtenerInformacion('/getTracks/')
          .timeout(ENV.maxTimeOut)
          .subscribe(
            (data)=> {this.tracks2 = data['tracks'];
            this.proveedor.obtenerInformacion('/getsessions/')
            .timeout(ENV.maxTimeOut)
            .subscribe(
              (data)=> {this.sesiones2 = data['days'];this.filtrar_datos2();},
              (error)=> {}
            );
          },
            (error)=> {console.log(error);}
          );      
      },
        (error)=> {console.log(error);}
      );
    });

  }


  showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Tracks');
    
    this.filtros.forEach((e)=>{
      if(e.deleted != true){
        alert.addInput({
          type: 'checkbox',
          label: e.trackName,
          value: e.trackId,
          checked: e.checked
        });
      }
    });
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Filtrar',
      handler: data => {
        this.testCheckboxOpen = false;
        this.testCheckboxResult = data;
        for(let j = 0; j <this.filtros.length; j++){
          this.filtros[j]['checked'] = false;
        }
        for( let i = 0; i < this.testCheckboxResult.length; i++){
          for(let j = 0; j <this.filtros.length; j++){
            if(this.testCheckboxResult[i] == this.filtros[j].trackId ){
              this.filtros[j].checked = true;
            } 
          }
        }
        this.doRefresh(null);
      }
    });
    alert.present();
  }

  doRefresh(refresher) {
    this.inicializarParametros();
    if(refresher == null){
      this.loading = this.loadingCtrl.create({
        content: 'Cargando'
      });
      this.loading.present();
    }

    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers'];
      this.proveedor.obtenerInformacion('/getTracks/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.tracks = data['tracks'];
        this.proveedor.obtenerInformacion('/getsessions/')
        .timeout(ENV.maxTimeOut)
        .subscribe(
          (data)=> {this.sesiones = data['days']; this.filtrar_datos(); if(refresher){refresher.complete();};if(this.toast){this.toast.dismiss();};},
          (error)=> {console.log(error);this.error = 1; this.loading.dismiss();if(refresher){refresher.complete();}  }
        );
      },
        (error)=> {console.log(error);this.error = 1; this.loading.dismiss();}
      ); 
    },
      (error)=> {console.log(error);this.error = 1;this.loading.dismiss();}
    );
    if(refresher){
      setTimeout(() => {
        refresher.complete();
      }, 2000);
    }
  }

  inicializarParametros(){
    this.seleccionadoSegment = null;
    this.slides = null;
    this. page = "0";
    this.sesiones = null;
    this.eventos = null;
    this.fechas = new Array;
    this.event =  new Array;
    this.conferencista = new Array;
    this.tracks= new Array;
    this.confer = new Array;
    this.params = null;
    this.error = null;
    this.title = new Array;
    this.groupSesion = [];
    this.error = null;
  }

  ionViewDidLeave(){
    console.log("sale del home");
    this.refreshInfo.unsubscribe();
  }

  ionViewDidLoad(){

    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers']; this.proveedor.obtenerInformacion('/getTracks/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.tracks = data['tracks'];
        this.filtros = this.tracks;
        this.filtros.forEach( (e)=>{
            e['checked'] = false;
        });
        this.proveedor.obtenerInformacion('/getsessions/')
        .timeout(ENV.maxTimeOut)
        .subscribe(
          (data)=> {this.sesiones = data['days'];console.log(this.tracks); this.filtrar_datos();},
          (error)=> {console.log(error);this.error = 1; this.loading.dismiss();}
        )
      },
        (error)=> {console.log(error);this.error = 1;this.loading.dismiss();}
      );
      ;},
      (error)=> {console.log(error);this.error = 1;this.loading.dismiss();}
    )
    
  }

  ngAfterViewInit() {
    this.slider.autoHeight = true;   
  }
  
  public filtrar_datos(){
    let cont;
    let flag = false;
    let collator = new Intl.Collator(undefined, {
      numeric: true,
      sensitivity: 'base'
    });
    for (var i=0; i < this.sesiones.length; i++){
      for (var j=0; j < this.sesiones[i].events.length; j++){ 
        if(this.sesiones[i].events[j]){
          if (this.sesiones[i].events[j].deleted == true){
              delete this.sesiones[i].events[j];
          }else{
              if(this.testCheckboxResult.length > 0){
              for(var k = 0; k < this.testCheckboxResult.length; k++){
                  if(this.sesiones[i].events[j].track == this.testCheckboxResult[k]){
                    flag = true;
                  }
              }
              if(!flag){ delete this.sesiones[i].events[j];}
              flag= false;
            }
          }
        }
      }
    }
    for (i=0; i < this.sesiones.length; i++){
        cont=0;
      for (j=0; j < this.sesiones[i].events.length; j++){ 
        if(this.sesiones[i].events[j] === undefined){
          cont++;
        }
      }
      if (cont == this.sesiones[i].events.length){
        delete this.sesiones[i];
      }else{
        this.sesiones[i] = _.sortBy(this.sesiones[i],['from','to'],'asc');
      }
    }
      
    this.sesiones = this.sesiones.filter(val => !!val);
    this.sesiones.sort(function(a, b) { a[0] = a[0].split('-').reverse().join(''); b[0] = b[0].split('-').reverse().join(''); return a[0].localeCompare(b[0]);});
    if(this.sesiones.length == 1){
      this.sesiones[0][0] = this.sesiones[0][0].split('-').reverse().join('');
    }
    for( i = 0; i < this.sesiones.length; i++){
      if(this.sesiones[i][0] != undefined){
        this.fechas.push(this.diaSemana(this.sesiones[i][0]) + ' ' + this.sesiones[i][0].substr(6, 2) );
        this.event.push(this.sesiones[i][1]);
      }
    }

    for ( i = 0; i < this.event.length; i++){
      this.event[i] = this.event[i].filter(val => !!val);
    }

    for(i = 0 ; i < this.event.length ; i++ ){
      this.event[i].sort(function(a, b) { let an = new Date(a.from); let bn = new Date(b.from); return (an<bn ? -1 : an>bn ? 1 : 0 || collator.compare(a.name, b.name)) });
    }

    for ( i = 0; i < this.event.length; i++){
      for( j = 0; j < this.event[i].length; j++){
        var dateInicio = new Date(this.event[i][j]['from']);
        var dateFinal = new Date(this.event[i][j]['to']);
        if(isNaN(dateInicio.getHours()) == false){
          this.event[i][j]['from'] = dateInicio.getHours() + ':' + (dateInicio.getMinutes()<10?'0':'') + dateInicio.getMinutes();
          this.event[i][j]['to'] = dateFinal.getHours() + ':' + (dateFinal.getMinutes()<10?'0':'') + dateFinal.getMinutes(); 
        }
        if((this.event[i][j]['speakers']).length > 0){
          this.event[i][j]['speakersNames'] = new Array;
          this.event[i][j]['speakersImagen'] = new Array;
          for( var k = 0; k < this.conferencista.length; k++){
            for(var l = 0; l < (this.event[i][j]['speakers']).length ; l++){
              if(((this.conferencista[k]['speakerId']) == (this.event[i][j]['speakers'][l])) && this.conferencista[k]['deleted'] == false){
                  this.event[i][j]['speakersNames'][l] = ( ' ' + this.conferencista[k]['firstName'] + ' ' +this.conferencista[k]['lastName']);
                  this.event[i][j]['speakersImagen'][l] = (this.conferencista[k]['avatarImageURL']);
                }
              }
            }
          }

          for( k = 0; k < this.tracks.length; k++){
            if((this.tracks[k]['trackId']) == (this.event[i][j]['track'])){
              if(this.tracks[k]['deleted'] !=  true){
                  this.event[i][j]['trackN'] = this.tracks[k]['trackName'];
                  k = this.tracks.length - 1;
              }
            }
          }

        }
      }
      this.groupEventos(this.event);
      this.loading.dismiss();
      
  }

  public filtrar_datos2(){
    let cont;
    let flag = false;
    let collator = new Intl.Collator(undefined, {
      numeric: true,
      sensitivity: 'base'
    });
   
    for (var i=0; i < this.sesiones2.length; i++){
      for (var j=0; j < this.sesiones2[i].events.length; j++){ 
        if(this.sesiones2[i].events[j]){
          if (this.sesiones2[i].events[j].deleted == true){
              delete this.sesiones2[i].events[j];
          }else{
            if(this.testCheckboxResult){
              if(this.testCheckboxResult.length > 0){
                for(var k = 0; k < this.testCheckboxResult.length; k++){
                    if(this.sesiones2[i].events[j].track == this.testCheckboxResult[k]){
                      flag = true;
                    }
                }
              if(!flag){ delete this.sesiones2[i].events[j];}
              flag= false;
              }
            }
          }
        }
      }
    }
    for (i=0; i < this.sesiones2.length; i++){
        cont=0;
      for (j=0; j < this.sesiones2[i].events.length; j++){ 
        if(this.sesiones2[i].events[j] === undefined){
          cont++;
        }
      }
      if (cont == this.sesiones2[i].events.length){
        delete this.sesiones2[i];
      }else{
        this.sesiones2[i] = _.sortBy(this.sesiones2[i],['from','to'],'asc');
      }
    }

    this.sesiones2 = this.sesiones2.filter(val => !!val);
    this.sesiones2.sort(function(a, b) { a[0] = a[0].split('-').reverse().join(''); b[0] = b[0].split('-').reverse().join(''); return a[0].localeCompare(b[0]);});
    if(this.sesiones){
      if(this.sesiones.length == 1){
        this.sesiones[0][0] = this.sesiones[0][0].split('-').reverse().join('');
      }
    }

    for( i = 0; i < this.sesiones2.length; i++){
      if(this.sesiones2[i][0] != undefined){
        this.fechas2.push(this.diaSemana(this.sesiones2[i][0]) + ' ' + this.sesiones2[i][0].substr(6, 2) );
        this.event2.push(this.sesiones2[i][1]);
      }
    }

    for ( i = 0; i < this.event2.length; i++){
      this.event2[i] = this.event2[i].filter(val => !!val);
    }
      //Orden Natural
    for(i = 0 ; i < this.event2.length ; i++ ){
      this.event2[i].sort(function(a, b) { let an = new Date(a.from); let bn = new Date(b.from); return (an<bn ? -1 : an>bn ? 1 : 0 || collator.compare(a.name, b.name)) });
    }

    for ( i = 0; i < this.event2.length; i++){
      for( j = 0; j < this.event2[i].length; j++){
        var dateInicio = new Date(this.event2[i][j]['from']);
        var dateFinal = new Date(this.event2[i][j]['to']);
        if(isNaN(dateInicio.getHours()) == false){
          this.event2[i][j]['from'] = dateInicio.getHours() + ':' + (dateInicio.getMinutes()<10?'0':'') + dateInicio.getMinutes();
          this.event2[i][j]['to'] = dateFinal.getHours() + ':' + (dateFinal.getMinutes()<10?'0':'') + dateFinal.getMinutes(); 
        }
        if((this.event2[i][j]['speakers']).length > 0){
          this.event2[i][j]['speakersNames'] = new Array;
          this.event2[i][j]['speakersImagen'] = new Array;
          for( var k = 0; k < this.conferencista2.length; k++){
            for(var l = 0; l < (this.event2[i][j]['speakers']).length ; l++){
              if(((this.conferencista2[k]['speakerId']) == (this.event2[i][j]['speakers'][l])) && this.conferencista2[k]['deleted'] == false){
                  this.event2[i][j]['speakersNames'][l] = ( ' ' + this.conferencista2[k]['firstName'] + ' ' +this.conferencista2[k]['lastName']);
                  this.event2[i][j]['speakersImagen'][l] = (this.conferencista2[k]['avatarImageURL']);
                }
              }
            }
          }

          for( k = 0; k < this.tracks2.length; k++){
            if((this.tracks2[k]['trackId']) == (this.event2[i][j]['track'])){
              if(this.tracks2[k]['deleted'] !=  true){
                  this.event2[i][j]['trackN'] = this.tracks2[k]['trackName'];
                  k = this.tracks2.length - 1;
              }
            }
          }

        }
      }
      this.groupEventos2(this.event2);

      console.log(this.event,this.event2);
     if(this.compararEventos(this.event,this.event2)){
       if(!this.toast){
          this.presentToast();
       }
     }
      
  }

  public presentToast() {
    this.toast = this.toastCtrl.create({
      message: '¡Se actualizó la información del evento!',
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'x'
    });
    this.toast.onDidDismiss(() => {
      this.toast = null;
    });
    this.toast.present();
  }

  public selectedTab(index) {
      this.slider.slideTo(index);
      
  }
  
  public goTo(sbf) {
    sbf = sbf || null;
    this.navCtrl.push(DetalleSPage, {
      data: sbf
    });
  }

  public compararEventos(a,b){
    if(a.length != b.length) return true;

    for( var i = 0;i < a.length; i++){
      if(a[i].length != b[i].length) return true;
      for( var j = 0; j < a[i].length; j++){
        if(a[i][j].length != b[i][j].length) return true;
        for (let a_variable in a[i][j]) {
            if(a[i][j][a_variable] != b[i][j][a_variable]){
                if( a_variable == "speakers" || a_variable == "speakersImagen" || a_variable == "speakersNames"){
                  if(a[i][j][a_variable].length == b[i][j][a_variable].length){
                    for( var k = 0; k < a[i][j][a_variable].length ; k++){
                      if (a[i][j][a_variable][k] != b[i][j][a_variable][k]){
                        console.log(a[i][j][a_variable], b[i][j][a_variable]);
                        return true;
                      }
                    }
                  }else{
                    console.log(a[i][j][a_variable], b[i][j][a_variable]);
                    return true;
                  }
              }else{
                console.log(a[i][j][a_variable], b[i][j][a_variable]);
                return true;
              }
            }
        }
      }
    }
    return false;
  }


  public selecOpcion(index, e) {  
    if (this.fechas[index] != null) {
      this.seleccionadoSegment = this.fechas[index]; 
      //por alguna razon solo funciona con "tap" or "click" y no con "ionSelected" 
      let segments = e.target.parentNode.children;
      console.log(e);
      let len = segments.length;
      for (let i=0; i < len; i++) {
        segments[i].classList.remove('segment-activated');
      }
      e.target.classList.add('segment-activated');
      }
  }

  public diaSemana(x) {
    x = ( x.substr(0, 4) +'/'+ x.substr(4, 2) +'/'+ x.substr(6, 2));
    let date = new Date(x);
    let dia;
  
    let options = {
      weekday: 'long',
    };
    dia = date.toLocaleDateString(ENV.nombredia, options);
    
    if(dia =='lunes') return 'Lun';
    else if(dia =='martes') return 'Mar';
    else if(dia =='miércoles') return 'Mié';
    else if(dia =='jueves') return 'Jue';
    else if(dia =='viernes') return 'Vie';
    else if(dia =='sábado') return 'Sab';
    else if(dia =='domingo') return 'Dom';
    console.log(dia);
  
  }

  public groupEventos2(c){
    var i, j;
    let fecha = "";
    for ( i = 0; i < c.length ; i++){
      this.title2[i] = new Array;
        for(j = 0; j < c[i].length; j++){
          if(fecha != (c[i][j]['from'] + " - " + c[i][j]['to'])){
            fecha = (c[i][j]['from'] + " - " + c[i][j]['to']);
            this.title2[i].push(c[i][j]['from'] + " - " + c[i][j]['to']);
          }else{
            this.title2[i].push(null);
          }
        }
    }    


  }


  public abrirFiltros(){
    this.showCheckbox();
  }

  public groupEventos(c){
    var i, j;
    let fecha = "";
    for ( i = 0; i < c.length ; i++){
      this.title[i] = new Array;
      fecha = "";
        for(j = 0; j < c[i].length; j++){
          if(fecha != (c[i][j]['from'])){
            fecha = (c[i][j]['from']);
            this.title[i].push(c[i][j]['from']);
          }else{
            this.title[i].push(null);
          }
        }
    }    
  }

  public slideChanged() {
    if(this.fechas.length){
      if(this.searchElementRef){
        if(this.slider){
        let currentIndex = this.slider.getActiveIndex();
          if(!this.searchElementRef.nativeElement.parentNode.children[currentIndex]){currentIndex = 0;}
            if(!( this.searchElementRef.nativeElement.parentNode.children[currentIndex].classList[1] == "segment-activated" )){
              if(currentIndex < this.fechas.length){
                for( var i = 0; i < this.fechas.length; i++){
                  if( i != currentIndex){
                    this.searchElementRef.nativeElement.parentNode.children[i].classList.remove('segment-activated');
                  }
                }    
                this.searchElementRef.nativeElement.parentNode.children[currentIndex].classList.add('segment-activated');
              }
              this.slider.update();
              this.slider.slideTo(currentIndex,0,true);
            }
        }
      }
    }
  }
  
}
