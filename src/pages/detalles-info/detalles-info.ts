import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetallesInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detalles-info',
  templateUrl: 'detalles-info.html',
})
export class DetallesInfoPage {
  info: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.info = navParams.get('data');
    console.log(this.info);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetallesInfoPage');
  }

}
