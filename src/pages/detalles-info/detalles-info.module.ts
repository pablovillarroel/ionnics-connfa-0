import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetallesInfoPage } from './detalles-info';

@NgModule({
  declarations: [
    DetallesInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetallesInfoPage),
  ],
})
export class DetallesInfoPageModule {}
