import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';

import { environment as ENV } from '../../environments/environment' ;
import _ from 'lodash';
import leaflet from 'leaflet';

declare var openMap: any;
/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {
  
  @ViewChild('mapid') mapRef: ElementRef;

  public someData : Array<{address: string, latitude: number, longitude : number, locationId: number, locationName: string }>;
  public planos: any;
  public location = new leaflet.LatLng(40.737, -73.923);
  error = null;
  public claveMapa:string= ENV.api_Google_map;
  map: any;
  mapas: any;
  info: any;
  public nombreMapa = "No hay mapas";
  public direccion = " ";


  constructor(public navCtrl: NavController,public proveedor: ConexionApiProvider, public navParams: NavParams) {
    this.someData = [];
    this.location = new leaflet.LatLng(40.737, -73.923);
  }

  ionViewDidLoad() {
    this.proveedor.obtenerInformacion('/getLocations/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.mapas = data['locations']; this.filtrar_datos();},
      (error)=> {console.log(error);this.error = 1;this.funcion_error();}
    );
    
  }

  funcion_error(){
    
  }

  public mostarMapa(){
    this.map.setView(this.location,17);
    leaflet.marker(this.location, {draggable: false}).addTo(this.map);
  /*  const options = {
        center: this.location,
        zoom: 17,
        streetViewControl: false
    }
    const map = new google.maps.Map(this.mapRef.nativeElement, options);

    this.ponerMarcador(this.location, map);*/
  }

  public ponerMarcador(posicion, map){
    /*return new google.maps.Marker({
      position: posicion,
      map: map,
    });*/

  }

  public filtrarMapa(id){
    for( var i=0; i <this.someData.length; i++){
      if(id == this.someData[i]['locationId']){
          this.location = new leaflet.LatLng(this.someData[i]['latitude'], this.someData[i]['longitude']);
          this.mostarMapa();
          this.nombreMapa = this.someData[i]['locationName'];
          this.direccion = this.someData[i]['address'];
      }
    }
    
  }

  ionViewDidEnter() {
    
  }

  public filtrar_datos(){

    for (var i = 0; i < this.mapas.length; i++){
      if(this.mapas[i]['deleted'] == true){
        delete this.mapas[i];
      }
    }

    this.mapas = this.mapas.filter(val => !!val);
    this.mapas = _.orderBy(this.mapas,['locationId'],'asc') ;

    for (i = 0; i < this.mapas.length; i++){
      this.someData[i] =  this.mapas[i];
    }

    this.map = leaflet.map('mapid',{
      center:[23.03, -81.57],
      zoom: 17
    });
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
    leaflet.control.scale().addTo(this.map);

    if(this.someData.length){
      this.location = new leaflet.LatLng(this.someData[0]['latitude'], this.someData[0]['longitude']);
      this.mostarMapa();
      this.nombreMapa = this.someData[0]['locationName'];
      this.direccion = this.someData[0]['address'];
    }
       
  }

}

