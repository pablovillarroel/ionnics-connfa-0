import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConferencistaPage } from './conferencista';

@NgModule({
  declarations: [
    ConferencistaPage,
  ],
  imports: [
    IonicPageModule.forChild(ConferencistaPage),
  ],
})
export class ConferencistaPageModule {}
