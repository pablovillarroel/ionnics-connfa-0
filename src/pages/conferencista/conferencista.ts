import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController  } from 'ionic-angular';

import { DetalleConfPage } from '../detalle-conf/detalle-conf';

import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';

import { environment as ENV } from '../../environments/environment' ;

import _ from 'lodash';
import { Observable }   from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";



/**
 * Generated class for the ConferencistaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-conferencista',
  templateUrl: 'conferencista.html',
})
export class ConferencistaPage {
  conferencista = new Array;
  conferencista2 = new Array;
  nombreCompleto = new Array;
  tracks= new Array;
  confer = new Array;
  params: any;
  error = null;
  contactos : any;
  imagenes = new Array;
  ids = new Array;
  characteristic = new Array;
  email = new Array;
  jobTitle = new Array;
  organizationName = new Array;
  twitterName = new Array;
  webSite = new Array;
  groupedContacts = [];
  ses: any;
  loading :any;
  observableVar: Subscription;
  refreshInfo: Subscription;
  toast = null;
 
  constructor(private toastCtrl: ToastController, public navCtrl: NavController,public loadingCtrl: LoadingController, public proveedor: ConexionApiProvider,public navParams: NavParams) {
    this.presentLoadingDefault();
    this.refreshInfo = Observable.interval(20000).subscribe(()=>{
      this.conferencista2 = new Array;
      this.proveedor.obtenerInformacion('/getSpeakers/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.conferencista2 = data['speakers']; this.filtrardatos2(); },
        (error)=> {console.log(error);}
      )
    });
  }

  ionViewDidLeave(){
    console.log("sale del home");
    this.refreshInfo.unsubscribe();
  }

  ionViewDidLoad() {
    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers']; this.filtrardatos(); },
      (error)=> {console.log(error);  this.error = 1; this.loading.dismiss();}
    )
  }

  inicializarParametros(){
    this.conferencista = new Array;
    this.nombreCompleto = new Array;
    this.tracks= new Array;
    this.confer = new Array;
    this.error = null;
    this.imagenes = new Array;
    this.ids = new Array;
    this.characteristic = new Array;
    this.email = new Array;
    this.jobTitle = new Array;
    this.organizationName = new Array;
    this.twitterName = new Array;
    this.webSite = new Array;
    this.groupedContacts = new Array;
    
  }

  doRefresh(refresher) {
    this.inicializarParametros();
    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> { this.conferencista = data['speakers']; this.filtrardatos(); refresher.complete(); if(this.toast){this.toast.dismiss()};},
      (error)=> {console.log(error);  this.error = 1;}
    )
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Cargando'
    });
    this.loading.present();
  }

  public filtrardatos(){

    for (var i = 0; i < this.conferencista.length; i++){
      if(this.conferencista[i]['deleted'] == true){
        delete this.conferencista[i];
      }
    }

    this.conferencista = this.conferencista.filter(val => !!val);
    this.conferencista = _.orderBy(this.conferencista,['firstName','lastName'],'asc') ;
   
    for (i=0; i < this.conferencista.length; i++){
      this.nombreCompleto.push(this.conferencista[i]['firstName'] + ' ' +this.conferencista[i]['lastName']);
      this.imagenes.push(this.conferencista[i]['avatarImageURL']);
      this.ids.push((this.conferencista[i]['speakerId']));
      this.characteristic.push(this.conferencista[i]['characteristic']);
      this.email.push(this.conferencista[i]['email']);
      this.jobTitle.push(this.conferencista[i]['jobTitle']);
      this.organizationName.push(this.conferencista[i]['organizationName']);
      this.twitterName.push(this.conferencista[i]['twitterName']);
      this.webSite.push(this.conferencista[i]['webSite']);
    }

    this.groupContacts(this.nombreCompleto);
    this.loading.dismiss();
    
    
    
  }
  public filtrardatos2(){

    for (var i = 0; i < this.conferencista2.length; i++){
      if(this.conferencista2[i]['deleted'] == true){
        delete this.conferencista2[i];
      }
    }

    this.conferencista2 = this.conferencista2.filter(val => !!val);
    this.conferencista2 = _.orderBy(this.conferencista2,['firstName','lastName'],'asc') ;

    if(this.compararConfer(this.conferencista,this.conferencista2)){
      console.log(this.conferencista,this.conferencista2);
      if(!this.toast){
        this.presentToast();
      }
    }
  }

  public presentToast() {
    this.toast = this.toastCtrl.create({
      message: '¡Se actualizó la información de los conferencistas! Refresca deslizando hacia abajo',
      position: 'top',
      showCloseButton: true,
      dismissOnPageChange: true,
      closeButtonText: "x"
    });
    this.toast.onDidDismiss(() => {
      this.toast = null;
    });
    this.toast.present();
  }

  public compararConfer(a,b){
    if(a.length != b.length) return true;
    for ( var i = 0; i < a.length; i++){
      for (let a_variable in a[i]){
          if(a[i][a_variable] != b[i][a_variable]){
           return true;
          }
      }
    }
    return false;
  }

  public goTo(ses,n) {
    let conf = [];
    ses = ses || null;
    if(ses){
      
      conf['id'] = ses['id'][n];
      conf['contacts'] = ses['contacts'][n];
      conf['images'] = ses['images'][n];
      conf['characteristic'] = ses['characteristic'][n];
      conf['jobTitle'] = ses['jobTitle'][n];
      conf['organizationName'] = ses['organizationName'][n]; 
      
      this.navCtrl.push(DetalleConfPage, {
        data: conf
      });

    }
  }


 public groupContacts(c){
    let letra = false;
    let contacto_actual = [];
    let imagen_actual = [];
    let id_c = [];
    let chara = [];
    let ema = [];
    let job = [];
    let orga = [];
    let twit = [];
    let web = [];

    c.forEach((value, index) => {

        if(value.charAt(0).toUpperCase() != letra){
            letra = value.charAt(0).toUpperCase();
            let nuevoGrupo = {
                letter: letra,
                contacts: new Array,
                images: new Array,
                id: new Array,
                characteristic: new Array,
                email: new Array,
                jobTitle: [],
                organizationName: new Array,
                twitterName: new Array,
                webSite: new Array
            };
            contacto_actual = nuevoGrupo.contacts;
            imagen_actual = nuevoGrupo.images;
            id_c = nuevoGrupo.id;
            chara = nuevoGrupo.characteristic;
            ema = nuevoGrupo.email;
            job = nuevoGrupo.jobTitle;
            orga = nuevoGrupo.organizationName;
            twit = nuevoGrupo.twitterName;
            web = nuevoGrupo.webSite;
            this.groupedContacts.push(nuevoGrupo);
            console.log(this.groupedContacts);
        }
        contacto_actual.push(value);
        imagen_actual.push(this.imagenes[index]);
        id_c.push(this.ids[index]);
        chara.push(this.characteristic[index]);
        ema.push(this.email[index]);;
        job.push(this.jobTitle[index]);
        orga.push(this.organizationName[index]);
        twit.push(this.twitterName[index]);
        web.push(this.webSite[index]);

    });
}

}
