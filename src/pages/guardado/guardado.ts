import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, SegmentButton, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';

import _ from 'lodash';
import 'rxjs/add/operator/timeout';
import { Observable }   from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";

import { environment as ENV } from '../../environments/environment' ;
import { DetalleSPage } from '../detalle-s/detalle-s';

/**
 * Generated class for the GuardadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-guardado',
  templateUrl: 'guardado.html',
})
export class GuardadoPage {
  @ViewChild(Slides) slider: Slides;
  @ViewChild(SegmentButton, { read: ElementRef } ) searchElementRef: ElementRef;
  @ViewChild(SegmentButton) botonesFecha: SegmentButton;
  seleccionadoSegment: string;
  slides: any;
  page: string = "0";
  sesiones: any;
  sesiones2 =  new Array;
  fechas = new Array;
  event =  new Array;
  conferencista = new Array;
  tracks= new Array;
  confer = new Array;
  params: any;
  error = null;
  public title = new Array;
  groupSesion = [];
  loading = this.loadingCtrl.create({
    content: 'Cargando'
  });
  eventosGuardados = new Array;
  observableVar: Subscription;
 

  constructor( public loadingCtrl: LoadingController, public navCtrl: NavController,public proveedor: ConexionApiProvider, public storage: Storage, public navParams: NavParams) {
    this.getAll();
    this.presentLoadingDefault();
    this.observableVar = Observable.interval(1000).subscribe(()=>{
      this.slideChanged();
    });
  }

  public presentLoadingDefault() {
    this.loading.present();
  }


  
  async getAll(){
      return await new Promise((resolve )=>{
        this.storage.forEach((v,k)=>{
          this.eventosGuardados.push(v);
        }).then(()=>{
            resolve(this.eventosGuardados);
            this.obtenerDatos();
          })
        })
    }

 ionViewDidLoad() {
  
  }

  async obtenerDatos(){
    this.tracks = new Array;
    this.sesiones = new Array;
    this.event = new Array;
    this.title = new Array ;
    this.fechas = new Array;
    
    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers'];},
      (error)=> {console.log(error); this.error = 1;}
    );
 
    if(this.error == 1){
      this.loading.dismiss();
      }else{this.proveedor.obtenerInformacion('/getTracks/')
        .timeout(ENV.maxTimeOut)
        .subscribe(
          (data)=> {this.tracks = data['tracks'];},
          (error)=> {console.log(error);  this.error = 1;}
        );
 
        if(this.error == 1){
          this.loading.dismiss();
        }else{
          this.proveedor.obtenerInformacion('/getsessions/')
          .timeout(ENV.maxTimeOut)
          .subscribe(
            (data)=> {this.sesiones = data['days'];this.filtrar_datos();},
            (error)=> {console.log(error); this.loading.dismiss();}
          );
        }
      }
  }

  ngAfterViewInit() {
    this.slider.autoHeight = true
    console.log(this.slider.isBeginning());
   
  }

  public diaSemana(x) {
    console.log('fecha', x);
    x = ( x.substr(0, 4) +'/'+ x.substr(4, 2) +'/'+ x.substr(6, 2));
    console.log('fecha2', x);
    let date = new Date(x);
    let dia;
  
    let options = {
      weekday: 'long',
    };
    dia = date.toLocaleDateString(ENV.nombredia, options);
    
    if(dia =='lunes') return 'Lun';
    else if(dia =='martes') return 'Mar';
    else if(dia =='miércoles') return 'Mié';
    else if(dia =='jueves') return 'Jue';
    else if(dia =='viernes') return 'Vie';
    else if(dia =='sábado') return 'Sab';
    else if(dia =='domingo') return 'Dom';
    console.log(dia);
  
  }

  public async filtrar_datos(){
    let cont;
    let flag = false; ;
    let collator = new Intl.Collator(undefined, {
      numeric: true,
      sensitivity: 'base'
    });

   console.log(this.eventosGuardados,  this.eventosGuardados.length); 
      for (var i=0; i < this.sesiones.length; i++){
        for (var j=0; j < this.sesiones[i].events.length; j++){ 
            for(var k = 0; k < this.eventosGuardados.length; k++){
              console.log(this.sesiones[i].events[j], this.eventosGuardados[k]);
              if(this.sesiones[i].events[j].eventId == this.eventosGuardados[k]){
                  console.log(this.eventosGuardados[k], this.sesiones[i].events[j].eventId );
                 flag=true;
              }
            }
            if(flag == false){
              delete this.sesiones[i].events[j];
            }else{
              flag= false;
            }
        }
      } 
    for ( i=0; i < this.sesiones2.length; i++){
      for (j=0; j < this.sesiones2[i].events.length; j++){ 
        if (!this.sesiones2[i].events[j].Guardado){
          delete this.sesiones2[i].events[j];
        }
      }
    }

    for (i=0; i < this.sesiones.length; i++){
        cont=0;
      for ( j=0; j < this.sesiones[i].events.length; j++){ 
        if(this.sesiones[i].events[j] === undefined){
          cont++;
        }
      }
      if (cont == this.sesiones[i].events.length){
        delete this.sesiones[i];
      }else{
        this.sesiones[i] = _.sortBy(this.sesiones[i],['from','to'],'asc');
      }
    }
      
    this.sesiones = this.sesiones.filter(val => !!val);
    this.sesiones.sort(function(a, b) { a[0] = a[0].split('-').reverse().join(''); b[0] = b[0].split('-').reverse().join('');  return a[0].localeCompare(b[0]);});
    if(this.sesiones.length == 1){
      this.sesiones[0][0] = this.sesiones[0][0].split('-').reverse().join('');
    }
    for( i = 0; i < this.sesiones.length; i++){
      if(this.sesiones[i][0] != undefined){
        console.log("hola",this.sesiones[i]);
        this.fechas.push(this.diaSemana(this.sesiones[i][0]) + ' ' + this.sesiones[i][0].substr(6, 2) );
        this.event.push(this.sesiones[i][1]);
      }
    }

    for ( i = 0; i < this.event.length; i++){
      this.event[i] = this.event[i].filter(val => !!val);
    }

    for(i = 0 ; i < this.event.length ; i++ ){
      this.event[i].sort(function(a, b) { let an = new Date(a.from); let bn = new Date(b.from); return (an<bn ? -1 : an>bn ? 1 : 0 || collator.compare(a.name, b.name)) });
    }

    for ( i = 0; i < this.event.length; i++){
      for( j = 0; j < this.event[i].length; j++){
        var dateInicio = new Date(this.event[i][j]['from']);
        var dateFinal = new Date(this.event[i][j]['to']);
        if(isNaN(dateInicio.getHours()) == false){
          this.event[i][j]['from'] = dateInicio.getHours() + ':' + (dateInicio.getMinutes()<10?'0':'') + dateInicio.getMinutes();
          this.event[i][j]['to'] = dateFinal.getHours() + ':' + (dateFinal.getMinutes()<10?'0':'') + dateFinal.getMinutes(); 
        }
        if((this.event[i][j]['speakers']).length > 0){
          this.event[i][j]['speakersNames'] = new Array;
          this.event[i][j]['speakersImagen'] = new Array;
          for( var k = 0; k < this.conferencista.length; k++){
            for(var l = 0; l < (this.event[i][j]['speakers']).length ; l++){
              if(((this.conferencista[k]['speakerId']) == (this.event[i][j]['speakers'][l])) && this.conferencista[k]['deleted'] == false){
                  this.event[i][j]['speakersNames'][l] = (' ' +this.conferencista[k]['firstName'] + ' ' +this.conferencista[k]['lastName']);
                  this.event[i][j]['speakersImagen'][l] = (this.conferencista[k]['avatarImageURL']);
                }
              }
            }
          }

          for( k = 0; k < this.tracks.length; k++){
            if((this.tracks[k]['trackId']) == (this.event[i][j]['track'])){
              if(this.tracks[k]['deleted'] !=  true){
                  this.event[i][j]['trackN'] = this.tracks[k]['trackName'];
                  k = this.tracks.length - 1;
              }
            }
          }

        }
      }
      this.groupEventos(this.event);
      this.loading.dismiss();
  }

  public groupEventos(c){
    var i, j;
    let fecha = "";
    for ( i = 0; i < c.length ; i++){
      this.title[i] = new Array;
      fecha = "";
        for(j = 0; j < c[i].length; j++){
          if(fecha != (c[i][j]['from'])){
            fecha = (c[i][j]['from']);
            this.title[i].push(c[i][j]['from']);
          }else{
            this.title[i].push(null);
          }
        }
    }    
  }

  public slideChanged() {
    if(this.fechas.length){
      if(this.searchElementRef){
        if(this.slider){
        let currentIndex = this.slider.getActiveIndex();
          if(!this.searchElementRef.nativeElement.parentNode.children[currentIndex]){currentIndex = 0;}
            if(!( this.searchElementRef.nativeElement.parentNode.children[currentIndex].classList[1] == "segment-activated" )){
              if(currentIndex < this.fechas.length){
                for( var i = 0; i < this.fechas.length; i++){
                  if( i != currentIndex){
                    this.searchElementRef.nativeElement.parentNode.children[i].classList.remove('segment-activated');
                  }
                }    
                this.searchElementRef.nativeElement.parentNode.children[currentIndex].classList.add('segment-activated');
              }
              this.slider.update();
              this.slider.slideTo(currentIndex,0,true);
            }
        }
      }
    }
  }
  

public selectedTab(index) {
    this.slider.slideTo(index);
    
}

public goTo(sbf) {
  sbf = sbf || null;
  this.navCtrl.push(DetalleSPage, {
    data: sbf
  });
}

public selecOpcion(index, e) {  
  if (this.fechas[index] != null) {
    this.seleccionadoSegment = this.fechas[index]; 
    //por alguna razon solo funciona con "tap" or "click" y no con "ionSelected" 
    let segments = e.target.parentNode.children;
    console.log(e);
    let len = segments.length;
    for (let i=0; i < len; i++) {
      segments[i].classList.remove('segment-activated');
    }
    e.target.classList.add('segment-activated');
    }
}

}
