import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetalleConfPage } from '../detalle-conf/detalle-conf';
import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';
import { environment as ENV } from '../../environments/environment' ;
import { Storage } from '@ionic/storage';
import _ from 'lodash';

/**
 * Generated class for the DetalleSPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detalle-s',
  templateUrl: 'detalle-s.html',
})
export class DetalleSPage {
  imagenFondo = ENV.imagenSesion;
  iconoGuardado :any = "ios-star-outline";
  sesion: any;
  conferencista = new Array;
  nombreCompleto = new Array;
  tracks= new Array;
  confer = new Array;
  params: any;
  error = null;
  contactos : any;
  imagenes = new Array;
  ids = new Array;
  characteristic = new Array;
  email = new Array;
  jobTitle = new Array;
  organizationName = new Array;
  twitterName = new Array;
  webSite = new Array;

  constructor(public storage: Storage,public navCtrl: NavController, public proveedor: ConexionApiProvider, public navParams: NavParams) {
    this.sesion = navParams.get('data');
  }

  ionViewDidLoad() {
    console.log("ID",this.sesion['eventId']);
    this.storage.get(('eventid'+this.sesion['eventId'])).then((e) =>{
      console.log(e);
      if(!e){
        this.iconoGuardado = "ios-star-outline";
      }else{
        this.iconoGuardado = "md-star";
      }
    });    
  
  }
 
  public goTo(ses) {
    ses = ses || null;
    console.log(ses);
    for ( let i=0; i < this.navCtrl.length(); i++ )
    {
        let v = this.navCtrl.getViews()[i];
        console.log(v.component.name);
        if(v.component.name == "DetalleConfPage"){
          this.navCtrl.remove(i);
        }
    }
    this.navCtrl.push(DetalleConfPage, {
      data: ses
    });
  }

  public filtrardatos(numId){

    var datos = new Object();
    datos = {};
    
    for (var i = 0; i < this.conferencista.length; i++){
      if(this.conferencista[i]['deleted'] == true){
        delete this.conferencista[i];
      }
    }
    
    this.conferencista = this.conferencista.filter(val => !!val);
    this.conferencista = _.orderBy(this.conferencista,['firstName','lastName'],'asc');
   
    for (i = 0; i < this.conferencista.length; i++){
      if((this.conferencista[i]['speakerId']) ==  numId){
        datos['characteristic'] = this.conferencista[i]['characteristic'];
        datos['contacts'] = this.conferencista[i]['firstName'] + ' ' + this.conferencista[i]['lastName'];
        datos['id'] = (this.conferencista[i]['speakerId']);
        datos['images'] = this.conferencista[i]['avatarImageURL'];
        datos['jobTitle'] = this.conferencista[i]['jobTitle'];
        datos['organizationName'] = this.conferencista[i]['organizationName'];
      }
    }  

    //this.navCtrl.pop();
    for ( let i=0; i < this.navCtrl.length(); i++ )
    {
        let v = this.navCtrl.getViews()[i];
        console.log(v.component.name);
        if(v.component.name == "DetalleConfPage"){
          this.navCtrl.remove(i);
        }
    }
    
    this.navCtrl.push(DetalleConfPage, {
      data: datos
    });
    
  }

  public goConf(ses){
   
    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers']; this.filtrardatos(ses);},
      (error)=> {console.log(error);  this.error = 1; }
    )
  }

  public async guardar_sesion(id){
    this.obtener_sesion(id);
  }

  public async obtener_sesion(id){
    this.storage.get(('eventid'+id)).then((e) =>{
      if(e){
        this.storage.remove(('eventid'+ id));
        this.iconoGuardado = "ios-star-outline";
      }else{
        this.storage.set(('eventid'+id), id);
        this.iconoGuardado = "md-star";
      }
    });    
  }

}
