import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalleSPage } from './detalle-s';

@NgModule({
  declarations: [
    DetalleSPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleSPage),
  ],
})
export class DetalleSPageModule {}
