import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides,LoadingController, SegmentButton  } from 'ionic-angular';

import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';

import { environment as ENV } from '../../environments/environment' ;

import { DetalleSPage } from '../../pages/detalle-s/detalle-s';

import _ from 'lodash';
import 'rxjs/add/operator/timeout';

/**
 * Generated class for the BofPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-evento',
  templateUrl: 'evento.html',
})
export class EventoPage {

  @ViewChild(Slides) slider: Slides;
  @ViewChild(SegmentButton) botonesFecha: SegmentButton;
  selectedSegment: string;
  slides: any;
  page = 0;
  sesiones: any;
  eventos:any;
  fechas = new Array;
  event =  new Array;
  conferencista = new Array;
  tracks= new Array;
  confer = new Array;
  params: any;
  error = null;
  groupSesion = [];
  loading = this.loadingCtrl.create({
    content: 'Cargando'
  });
  presentLoadingDefault() {
  
    this.loading.present();
  
  }
  constructor(public navCtrl: NavController, public proveedor: ConexionApiProvider, public loadingCtrl: LoadingController) { 
    
    this.presentLoadingDefault() ;
 

  }
  
  ionViewDidLoad(){

    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers'];},
      (error)=> {console.log(error);this.error = 1;}
    )
    this.proveedor.obtenerInformacion('/getTracks/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.tracks = data['tracks'];},
      (error)=> {console.log(error);this.error = 1;}
    )
    if(this.error == 1){
        this.loading.dismiss();
    }else{
      this.proveedor.obtenerInformacion('/getSocialEvents/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.sesiones = data['days']; this.filtrar_datos();},
        (error)=> {console.log(error);this.error = 1; this.loading.dismiss()}
      )
    }
  }

  ngAfterViewInit() {
    this.slider.autoHeight = true; 
    console.log(this.botonesFecha);
  }
  
  filtrar_datos(){
    let cont;
   
    for (var i=0; i < this.sesiones.length; i++){
      for (var j=0; j < this.sesiones[i].events.length; j++){ 
        if (this.sesiones[i].events[j].deleted == true){
          delete this.sesiones[i].events[j];
        }
      }
    }
    for (i=0; i < this.sesiones.length; i++){
        cont=0;
      for (j=0; j < this.sesiones[i].events.length; j++){ 
        if(this.sesiones[i].events[j] === undefined){
          cont++;
        }
      }
      if (cont == this.sesiones[i].events.length){
        delete this.sesiones[i];
      }else{
        this.sesiones[i] = _.orderBy(this.sesiones[i],'from','asc');
      }
    }
    
    this.eventos = this.sesiones.filter(val => !!val);

    this.eventos = _.orderBy(this.eventos,0,'asc');

    for( i = 0; i < this.eventos.length; i++){
      if(this.eventos[i][0] != undefined){
        this.fechas.push(this.eventos[i][0]);
        this.event.push(this.eventos[i][1]);
      }
    }
    for ( i = 0; i < this.event.length; i++){
      this.event[i] = this.event[i].filter(val => !!val);
    }

    for ( i = 0; i < this.event.length; i++){
      for( j = 0; j < this.event[i].length; j++){
        var dateInicio = new Date(this.event[i][j]['from']);
        var dateFinal = new Date(this.event[i][j]['to']);
        this.event[i][j]['from'] = dateInicio.getHours() + ':' + (dateInicio.getMinutes()<10?'0':'') + dateInicio.getMinutes();
        this.event[i][j]['to'] = dateFinal.getHours() + ':' + (dateFinal.getMinutes()<10?'0':'') + dateFinal.getMinutes(); 
        if((this.event[i][j]['speakers']).length > 0){
          this.event[i][j]['speakersNames'] = new Array;
          this.event[i][j]['speakersImagen'] = new Array;
          for( var k = 0; k < this.conferencista.length; k++){
            for(var l = 0; l < (this.event[i][j]['speakers']).length ; l++){
              if((this.conferencista[k]['speakerId']) == (this.event[i][j]['speakers'][l])){
                  this.event[i][j]['speakersNames'].push(this.conferencista[k]['firstName'] + ' ' +this.conferencista[k]['lastName']);
                  this.event[i][j]['speakersImagen'].push(this.conferencista[k]['avatarImageURL']);
                }
              }
            }
          }

          for( k = 0; k < this.tracks.length; k++){
            if((this.tracks[k]['trackId']) == (this.event[i][j]['track'])){
              if(this.tracks[k]['deleted'] !=  true){
                  this.event[i][j]['trackN'] = this.tracks[k]['trackName'];
                  k = this.tracks.length - 1;
              }
            }
          }

        }
      }
      for(i = 0 ; i < this.event.length ; i++ ){
        this.event[i] = _.orderBy(this.event[i],'from','asc');
      }
      this.loading.dismiss();   
      
  }
  public selectedTab(index) {
      this.slider.slideTo(index);
  }
  
  public goTo(sbf) {
    sbf = sbf || null;
    this.navCtrl.push(DetalleSPage, {
      data: sbf
    });
  }

  public selecOpcion(index, e) {
    if (this.fechas[index] != null) {
      this.selectedSegment = this.fechas[index]; 
      //por alguna razon solo funciona con "tap" or "click" y no con "ionSelected" 
      let segments = e.target.parentNode.children;
      let len = segments.length;
      for (let i=0; i < len; i++) {
        segments[i].classList.remove('segment-activated');
      }
      e.target.classList.add('segment-activated');
      }
  }  

}