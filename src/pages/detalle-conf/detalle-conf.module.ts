import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalleConfPage } from './detalle-conf';

@NgModule({
  declarations: [
    DetalleConfPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleConfPage),
  ],
})
export class DetalleConfPageModule {}
