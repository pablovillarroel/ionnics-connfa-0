import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController  } from 'ionic-angular';

import { environment as ENV } from '../../environments/environment' ;
import { ConferencistaPage } from '../conferencista/conferencista';

import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';
import _ from 'lodash';
import { DetalleSPage } from '../detalle-s/detalle-s';

/**
 * Generated class for the DetalleConfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detalle-conf',
  templateUrl: 'detalle-conf.html',
})
export class DetalleConfPage {
imagenFondo = ENV.imagenSesion;
conferencista = new Array;
tracks = new Array;
sesiones = new Array;
eventos = new Array;
event = new Array;
conferen:any;
ses: any;
ses1 = new Array;
ses2 = new Array;
sesi = new Array;
bof: any;
eve: any;
error = null;
traspaso = new Array;
traspaso2 = new Array;

  constructor(public viewCtrl:ViewController, public navCtrl: NavController, public proveedor: ConexionApiProvider, public navParams: NavParams) {
    this.conferen = navParams.get('data');    
  }


  public goConf(ses){
    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers'];
      this.proveedor.obtenerInformacion('/getTracks/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.tracks = data['tracks'];
        this.proveedor.obtenerInformacion('/getsessions/')
        .timeout(ENV.maxTimeOut)
        .subscribe(
      (data)=> {this.sesiones = data['days']; this.filtrardatos2(ses.eventId);},
      (error)=> {console.log(error);  this.error = 1; }
    );
        });
      });
  }
  public goBof(ses){

    this.proveedor.obtenerInformacion('/getSpeakers/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.conferencista = data['speakers']; },
      (error)=> {console.log(error);  this.error = 1; }
    );
    this.proveedor.obtenerInformacion('/getBofs/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.sesiones = data['days']; this.filtrardatos2(ses.eventId);},
      (error)=> {console.log(error);  this.error = 1; }
    );

  }

  public filtrardatos2(numId){
    console.log(numId);
    let cont;
   
    for (var i=0; i < this.sesiones.length; i++){
      for (var j=0; j < this.sesiones[i].events.length; j++){ 
        if (this.sesiones[i].events[j].deleted == true){
          delete this.sesiones[i].events[j];
        }
      }
    }
    for (i=0; i < this.sesiones.length; i++){
        cont=0;
      for (j=0; j < this.sesiones[i].events.length; j++){ 
        if(this.sesiones[i].events[j] === undefined){
          cont++;
        }
      }
      if (cont == this.sesiones[i].events.length){
        delete this.sesiones[i];
      }else{
        this.sesiones[i] = _.orderBy(this.sesiones[i],'from','asc');
      }
    }
      
    this.eventos = this.sesiones.filter(val => !!val);
    this.eventos = _.orderBy(this.eventos,0,'asc');
    this.sesiones = [];
    for( i = 0; i < this.eventos.length; i++){
      if(this.eventos[i][0] != undefined){
        this.event.push(this.eventos[i][1]);
      }
    }
    for ( i = 0; i < this.event.length; i++){
      this.event[i] = this.event[i].filter(val => !!val);
    }
    for(i = 0 ; i < this.event.length ; i++ ){
      this.event[i] = _.orderBy(this.event[i],['from','to'],'asc');
    }

    for ( i = 0; i < this.event.length; i++){
      for( j = 0; j < this.event[i].length; j++){
          var dateInicio = new Date(this.event[i][j]['from']);
          var dateFinal = new Date(this.event[i][j]['to']);
          if(isNaN(dateInicio.getHours()) == false){
            this.event[i][j]['from'] = dateInicio.getHours() + ':' + (dateInicio.getMinutes()<10?'0':'') + dateInicio.getMinutes();
            this.event[i][j]['to'] = dateFinal.getHours() + ':' + (dateFinal.getMinutes()<10?'0':'') + dateFinal.getMinutes(); 
          }
          if((this.event[i][j]['speakers']).length > 0){
            this.event[i][j]['speakersNames'] = new Array;
            this.event[i][j]['speakersImagen'] = new Array;
            for( var k = 0; k < this.conferencista.length; k++){
              for(var l = 0; l < (this.event[i][j]['speakers']).length ; l++){
                if(((this.conferencista[k]['speakerId']) == (this.event[i][j]['speakers'][l])) && this.conferencista[k]['deleted'] == false ){
                    this.event[i][j]['speakersNames'][l] = (this.conferencista[k]['firstName'] + ' ' + this.conferencista[k]['lastName']);
                    this.event[i][j]['speakersImagen'][l] = (this.conferencista[k]['avatarImageURL']);
                  }
                }
              }
            }
  
            for( k = 0; k < this.tracks.length; k++){
              if((this.tracks[k]['trackId']) == (this.event[i][j]['track'])){
                if(this.tracks[k]['deleted'] !=  true){
                    this.event[i][j]['trackN'] = this.tracks[k]['trackName'];
                    k = this.tracks.length - 1;
                }
              }
            }
        
      }
    }
    for( i = 0; i < this.event.length; i++){
      for( j = 0; j < this.event[i].length; j++){
        if(this.event[i][j]['eventId'] == numId){
          this.sesiones.push(this.event[i][j]);
        }
      }
    }
    for ( let i=0; i < this.navCtrl.length(); i++ )
    {
        let v = this.navCtrl.getViews()[i];
        console.log(v.component.name);
        if(v.component.name == "DetalleSPage"){
          this.navCtrl.remove(i);
        }
    }
    //this.viewCtrl.dismiss(); 
    this.navCtrl.push(DetalleSPage, {
      data: this.sesiones[0]
    });

      

  }

  ionViewDidLoad() {
    this.proveedor.obtenerInformacion('/getTracks/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.tracks = data['tracks'];
      this.proveedor.obtenerInformacion('/getSessions/')
      .timeout(ENV.maxTimeOut)
      .subscribe(
        (data)=> {this.ses = data['days']; this.filtrardatos();},
        (error)=> {console.log(error);  this.error = 1;}
       );
      });
    console.log("Todos los eventos");
    console.log(this.ses);
  }

  public diaSemana(x) {
    let date = new Date(x);
    let dia;
  
    let options = {
      weekday: 'long',
    };
    dia = date.toLocaleDateString(ENV.nombredia, options);
    
    if(dia =='lunes') return 'Lun';
    else if(dia =='martes') return 'Mar';
    else if(dia =='miércoles') return 'Mié';
    else if(dia =='jueves') return 'Jue';
    else if(dia =='viernes') return 'Vie';
    else if(dia =='sábado') return 'Sab';
    else if(dia =='domingo') return 'Dom';
  
  }

  public filtrardatos(){
    let contador = 0;
    for (var i=0; i < this.ses.length; i++){
      for (var j=0; j < this.ses[i].events.length; j++){ 
        if (this.ses[i].events[j].deleted == true){
          delete this.ses[i].events[j];
        }
      }
    }
    let cont;
    for (i=0; i < this.ses.length; i++){
      cont=0;
      for (j=0; j < this.ses[i].events.length; j++){ 
        if(this.ses[i].events[j] === undefined){
          cont++;
        }
      }
      if (cont == this.ses[i].events.length){
        delete this.ses[i];
      }else{
          this.ses[i] = _.orderBy(this.ses[i],'from','asc');
      }
    }

    this.ses = this.ses.filter(val => !!val);
    this.ses = _.orderBy(this.ses,0,'asc');

    for( i = 0; i < this.ses.length; i++){
      if(this.ses[i][0] != undefined){
        this.sesi.push(this.ses[i][1]);
      }
    }
    for ( i = 0; i < this.sesi.length; i++){
      this.sesi[i] = this.sesi[i].filter(val => !!val);
    }
    
    for ( i = 0; i < this.sesi.length; i++){
      for( j = 0; j < this.sesi[i].length; j++){
          var dateInicio = new Date(this.sesi[i][j]['from']);
          var dateFinal = new Date(this.sesi[i][j]['to']);
          if(isNaN(dateInicio.getHours()) == false){
            console.log("entre2");
            this.sesi[i][j]['from'] = this.diaSemana(dateInicio) + ', ' + dateInicio.getHours() + ':' + (dateInicio.getMinutes()<10?'0':'') + dateInicio.getMinutes();
            this.sesi[i][j]['to'] = dateFinal.getHours() + ':' + (dateFinal.getMinutes()<10?'0':'') + dateFinal.getMinutes();
          }
        for( var k = 0; k < this.sesi[i][j]['speakers'].length; k++){
          if(this.sesi[i][j]['speakers'][k] == this.conferen['id']){
            this.traspaso[contador] = new Array;
            this.traspaso[contador] = this.sesi[i][j];
            contador++;
          }
        }
      }
    }
  }

}
