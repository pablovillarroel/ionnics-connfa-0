import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConexionApiProvider } from '../../providers/conexion-api/conexion-api';

import { environment as ENV } from '../../environments/environment' ;
import { DetallesInfoPage } from '../detalles-info/detalles-info';
/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  error= null;
  listaInfo = new Array;

  constructor(public proveedor: ConexionApiProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.proveedor.obtenerInformacion('/getInfo/')
    .timeout(ENV.maxTimeOut)
    .subscribe(
      (data)=> {this.listaInfo = data['info']; this.filtrardatos(); },
      (error)=> {console.log(error);  this.error = 1;}
    )
  }

  public filtrardatos(){
    for(var i=0; i < this.listaInfo.length; i++){
      if(this.listaInfo[i].deleted){
        delete this.listaInfo[i];
      }
    }
    this.listaInfo = this.listaInfo.filter(val => !!val);
    console.log(this.listaInfo);
  }


  public goTo(info) {
    info = info || null;
    this.navCtrl.push(DetallesInfoPage, {
      data: info
    });
  }

}
