import { Component,ViewChild } from '@angular/core';
import { Platform,Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { environment as ENV } from '../environments/environment';

import { HomePage } from '../pages/home/home';
import { BofPage } from '../pages/bof/bof';
import { EventoPage } from '../pages/evento/evento';
import { ConferencistaPage } from '../pages/conferencista/conferencista';
import { PlanosPage } from '../pages/planos/planos';
import { MapaPage } from '../pages/mapa/mapa';
import { GuardadoPage } from '../pages/guardado/guardado';
import { InfoPage } from '../pages/info/info';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('NAV') nav : Nav; 
  rootPage:any = HomePage;
  imagenMenu :any = ENV.imagenMenu;

  public pages : Array<{titulo: string, componente: any, icono: string}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen ) {
    
    this.pages = [
      {titulo: "Sesiones", componente : HomePage , icono:"home"},
     // {titulo: "BoFs", componente : BofPage , icono:"person"},
      //{titulo: "Eventos", componente : EventoPage , icono:"mail"},
      {titulo: "Mis Eventos", componente : GuardadoPage , icono:"mail"},
      {titulo: "Conferencistas", componente : ConferencistaPage , icono:"person"},
     // {titulo: "Planos", componente: PlanosPage, icono:"person"},
      {titulo: "Mapas", componente: MapaPage, icono:"person"},
      {titulo: "Información", componente: InfoPage, icono:"person"}
    ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

  }

  public goToPage(page){
    this.nav.setRoot(page);
  }

}
