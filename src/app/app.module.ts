import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { BofPage } from '../pages/bof/bof';
import { EventoPage } from '../pages/evento/evento';
import { DetalleSPage } from '../pages/detalle-s/detalle-s';
import { DetalleConfPage } from '../pages/detalle-conf/detalle-conf';
import { ConferencistaPage } from '../pages/conferencista/conferencista';
import { PlanosPage } from '../pages/planos/planos';
import { MapaPage } from '../pages/mapa/mapa';
import { GuardadoPage } from '../pages/guardado/guardado';
import { InfoPage } from '../pages/info/info';

import { environment as ENV } from '../environments/environment' 

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HideHeaderDirective } from '../directives/hide-header/hide-header'
import { ConexionApiProvider } from '../providers/conexion-api/conexion-api';
import { DetallesInfoPage } from '../pages/detalles-info/detalles-info';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    BofPage,
    EventoPage,
    DetalleSPage,
    ConferencistaPage,
    DetalleConfPage,
    PlanosPage,
    MapaPage,
    GuardadoPage,
    InfoPage,
    DetallesInfoPage,
    HideHeaderDirective,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    BofPage,
    EventoPage,
    DetalleSPage,
    ConferencistaPage,
    DetalleConfPage,
    PlanosPage,
    MapaPage,
    InfoPage,
    DetallesInfoPage,
    GuardadoPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConexionApiProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
