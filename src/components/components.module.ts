import { NgModule } from '@angular/core';
import { ShrinkingSegmentHeaderComponent } from './shrinking-segment-header/shrinking-segment-header';
@NgModule({
	declarations: [ShrinkingSegmentHeaderComponent,
    ShrinkingSegmentHeaderComponent,
    ShrinkingSegmentHeaderComponent],
	imports: [],
	exports: [ShrinkingSegmentHeaderComponent,
    ShrinkingSegmentHeaderComponent,
    ShrinkingSegmentHeaderComponent]
})
export class ComponentsModule {}
